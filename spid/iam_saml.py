from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.settings import OneLogin_Saml2_Settings
from onelogin.saml2.utils import OneLogin_Saml2_Utilsimport

# take serv_id and idp_id from wsgi_server

#listen on localhost port 8055
# Facciamo finta che in qualche modo mi arrivano questi dati
service_identifier = 3
IDP_id = 238
target = "https://idp.spid.gov.it:8080/#/"

def init_saml_auth(req):
    auth = OneLogin_Saml2_Auth(req, custom_base_path=settings.SAML_FOLDER)
    return auth


def metadata(request):
    saml_settings = OneLogin_Saml2_Settings(settings=None, custom_base_path=settings.SAML_FOLDER, sp_validation_only=True)
    metadata = saml_settings.get_sp_metadata()
    errors = saml_settings.validate_metadata(metadata)

#@TO DO LATER
def listen_for_post(request):
    Returns


def saml_authentication_request(saml_request, relay_state):

    #@param saml_request (string) – The SAML Request
    #@param relay_state (string) – The target URL the user should be redirected to
    signature = build_request_signature(saml_request, relay_state)
