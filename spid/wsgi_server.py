#!/usr/bin/env python

from wsgiref.simple_server import make_server
from cgi import parse_qs, escape

html = """
<html>
<body>
   <form method="post" action="">
        <p>
           Service Provider Identification: <input type="text" name="serv_id" value="%(serv_id)s">
        </p>
        <p>
           Identity Provider Identification: <input type="text" name="idp_id" value="%(idp_id)s">
        </p>
        <p>
            <input type="submit" value="SPID Button">
        </p>
    </form>
    <p>
        Service Provider Identification: %(serv_id)s<br>
        Identity Provider Identification: %(idp_id)s
    </p>
</body>
</html>
"""

def application(environ, start_response):

    # the environment variable CONTENT_LENGTH may be empty or missing
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    # When the method is POST the variable will be sent
    # in the HTTP request body which is passed by the WSGI server
    # in the file like wsgi.input environment variable.
    request_body = environ['wsgi.input'].read(request_body_size)
    d = parse_qs(request_body)

    serv_id = d.get('serv_id', [''])[0] # Returns the first age value.
    idp_id = d.get('idp_id', [''])[0] # Returns the first age value.

    # Always escape user input to avoid script injection
    serv_id = escape(serv_id)
    idp_id = escape(idp_id)

    # SAML

    response_body = html % { # Fill the above html template in
        'serv_id': serv_id or 'Empty',
        'idp_id': idp_id or 'Empty',
    }

    status = '200 OK'

    response_headers = [
        ('Content-Type', 'text/html'),
        ('Content-Length', str(len(response_body)))
    ]


    start_response(status, response_headers)
    return response_body

httpd = make_server('localhost', 8051, application)
httpd.serve_forever()
