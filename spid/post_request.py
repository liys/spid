#!/usr/bin/python3
import requests
import http
import json
# defining the api-endpoint
ENDPOINT = "http://127.0.0.1:8051"
#ENDPOINT = "http://google.com:80"

service_identifier = 3
IDP_id = 238

# data to be sent to api

#body_response = application() to make connection
# sending post request and saving response as response object
data = {'service_identifier':service_identifier, 'idp_id':IDP_id}
r = requests.post(url = ENDPOINT, data = data)
print(r)
