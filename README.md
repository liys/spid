# Integrazione SPID

## INTRODUZIONE 
### 1.1. Introduzione 
 
AGID – Agenzia per l’Italia digitale intende realizzare una componente di integrazione per il progetto SPID da poter distribuire agli Enti che ne fanno richiesta in modo da facilitare il processo di integrazione del Sistema Pubblico di Identità Digitale come componente di autenticazione. 

### 1.2 Scopo del documento 

 Lo scopo del documento è quello di definire le linee guida di sviluppo dello Sprint n.1 (A3F) Il documento contiene la descrizione di:
 
 1.	Requisiti base 
 2. Obiettivi di sicurezza 
 3. Riferimenti ai protocolli utilizzati 
 4. Linee guida tecniche SPID 
 5. Flussi 

### 1.3 Requisiti base 
 
Lo Sprint n. 1 ha l’obiettivo di definire e realizzare la parte core base della soluzione secondo le seguenti caratteristiche base:

1. Gestione del metadata del Service Provider 
2. Gestione certificati di signing del metadata e delle asserzioni 
3. Registrazione Identity Provider accreditati 
4. Registrazione Applicazioni 
5. Registrazione Attibute Service e relativi Livelli di autenticazione 
6. Creazione di una SAML Authentication Request 
7. Gestione di una SAML Authentication Response 
8. Creazione di una SAML Logout Request 
9. Gestione di una SAML Logout Response 
10. Gestione richiesta di autenticazione ricevuta in HTTP Post dall’applicazione 
11. Gestione richiesta di logout ricevuta in HTTP Post dall’applicazione 
12. Retro compatibilità con l’attuale versione di spid-auth-docker 
13. Supporto multi-dominio 
14. Linguaggio di sviluppo 
15. Archiviazione dei dati di configurazione 

### 1.4 Obiettivi di sicurezza 
 
Per garentire un adeguato livello di sicurezza, tutte comunicazioni tra le componenti avverranno utilizzando il protocollo http in modalità sicura utilizzando il TLS più recente. Il metadata di definizione del Service Provider avrà una signing e le asserzioni nel colloquio tra Service Provider e Identity Provider avranno anche loro un signing basata su un certificato con chiavi RSA almeno a 1024 bit e algoritmo di digest SHA-256 o superiore 

### 1.5 Riferimenti ai protocolli utilizzati 
 
Relativamente all’interscambio tra le componenti coinvolte, devono essere presi a riferimento i protocolli standard di federazione che devono essere utilizzati. In questo primo Sprint deve essere preso a riferimento il protocollo SAML 2.0 secondo le specifiche Oasis (https://www.oasisopen.org/standards#samlv2.0) Per quanto riguarda l’archiviazione della configurazione si deve fare riferimento allo standard Yaml (https://yaml.org/spec/1.2/spec.html)
 
### 1.6 Linee guida tecniche SPID 
 
La realizzazione del primo Sprint deve tenere conto di quanto definito dalle specifiche tecniche SPID emanate da Agid, a tal scopo si deve fare riferimento a: https://www.agid.gov.it/sites/default/files/repository_files/circolari/spid-regole_tecniche_v1.pdf e la successiva emanazione https://docs.italia.it/italia/spid/spid-regole-tecniche/it/stabile/ 