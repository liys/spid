from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.settings import OneLogin_Saml2_Settings
from onelogin.saml2.utils import OneLogin_Saml2_Utils
from ruamel.yaml import YAML, yaml_object
import os,sys
from identityprovider import IdentityProvider
from cryptography import x509
import ntpath
import glob
import xml.etree.ElementTree as ET

#to create variable number of classes:
path = '/home/giulia/git/spid/metadata/'
#array of IdentityProvider objects class
#objects of idps
idps = []



def recursive_view(node):
    show = True
    for c in node.getchildren():
        show = False
        if('SingleSignOnService' in c.tag):
            print(c.attrib['Location'])
            print(c.attrib['Binding'])
        if('X509Certificate' in c.tag):
            print(c.tag,c.text)
        recursive_view(c)


def NewIDPFromXML(newfile):
    #parse xml document and create idp object
    #build the tree of the document
    tree = ET.parse(newfile)
    root = tree.getroot()
    idp = IdentityProvider()
    idp.setEntityId = root.attrib['entityID']
    #look to all its children
    recursive_view(root)


def loadIDPFromXMLFile(file):
    f = open(file,"r")
    #load the IdP
    idp = NewIDPFromXML(f)
    idps.append(idp)


def LoadIDPMetadata(xmlfiles):
    for file in xmlfiles:
        loadIDPFromXMLFile(file)

if __name__=='__main__':
    xmlfiles = []
    for file in glob.glob(path + "*.xml"):
        xmlfiles.append(file)
    LoadIDPMetadata(xmlfiles)
